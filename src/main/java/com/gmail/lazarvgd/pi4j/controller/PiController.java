package com.gmail.lazarvgd.pi4j.controller;

import com.pi4j.io.gpio.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by xor on 25.12.16..
 */

@RestController
public class PiController {

    private static GpioPinDigitalOutput pinDigitalOutput;

    @RequestMapping("/")
    public String hello(){

        return "Hello this is Pi Controller :)";
    }

    @RequestMapping("/light")
    public String light(){
        if(pinDigitalOutput==null){
            GpioController controller = GpioFactory.getInstance();
            pinDigitalOutput = controller.provisionDigitalOutputPin(RaspiPin.GPIO_21,"LED1", PinState.LOW);
        }
        pinDigitalOutput.toggle();

        return "Light changed";
    }


}
